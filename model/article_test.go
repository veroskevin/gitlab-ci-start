package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanCreateArticle(t *testing.T) {
	// Given
	name := "Lorem Ipsum"
	slug := "lorem-ipsum"
	body := "Lorem Ipsum Dolor Sit Amet"
	category := []string{"internet", "entertainment"}

	// When
	got, err := CreateArticle(slug, name, body, category...)

	// Then
	assert.Equal(t, slug, got.Slug)
	assert.Equal(t, name, got.Name)
	assert.Equal(t, body, got.Body)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Empty(t, err)
}

func TestCanCreateArticleWithOneCategory(t *testing.T) {
	// Given
	name := "Lorem Ipsum"
	slug := "lorem-ipsum"
	body := "Lorem Ipsum Dolor Sit Amet"
	category := []string{"internet", "entertainment"}

	// When
	got, err := CreateArticle(slug, name, body, category...)

	// Then
	assert.Equal(t, slug, got.Slug)
	assert.Equal(t, name, got.Name)
	assert.Equal(t, body, got.Body)
	assert.NotEmpty(t, got.CreatedAt)
	assert.Empty(t, err)
}

func TestCannotCreateArticleCategoryNotExist(t *testing.T) {
	// Given
	name := "Lorem Ipsum"
	slug := "lorem-ipsum"
	body := "Lorem Ipsum Dolor Sit Amet"

	// When
	got, err := CreateArticle(slug, name, body)

	// Then
	assert.Equal(t, "", got.Slug)
	assert.Equal(t, "", got.Name)
	assert.Equal(t, "", got.Body)
	assert.Empty(t, got.CreatedAt)
	assert.NotEmpty(t, err)
}
