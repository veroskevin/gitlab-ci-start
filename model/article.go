package model

import (
	"errors"
	"time"
)

// Article store an article
type Article struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Category  []string
}

var categoryEnum = []string{"news", "politic", "internet", "entertainment"}

func checkCategory(category []string) bool {
	for _, v1 := range categoryEnum {
		for _, v2 := range category {
			if v1 == v2 {
				return true
			}
		}
	}
	return false
}

// CreateArticle create an article
func CreateArticle(slug, name, body string, category ...string) (Article, error) {
	// Given some parameters: slug, name, body, category

	// When the category isn't provided
	if len(category) < 1 {
		return Article{}, errors.New("You don't provide any category")
	} else {
		// When the category is exist or it isn't
		if checkCategory(category) {
			// Then
			return Article{
				Slug:      slug,
				Name:      name,
				Body:      body,
				CreatedAt: time.Now(),
				Category:  category,
			}, nil
		} else {
			// Then
			return Article{}, errors.New("You input a wrong category")
		}
	}
}
