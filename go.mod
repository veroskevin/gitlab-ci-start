module gitlab.com/veroskevin/gitlab-ci-start

go 1.12

require (
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
)
